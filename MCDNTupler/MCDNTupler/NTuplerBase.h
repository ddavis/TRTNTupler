#ifndef MCDNTupler_NTuplerBase_h
#define MCDNTupler_NTuplerBase_h

// TRTFramework
#include <TRTFramework/AtlasIncludes.h>

class TTree;

namespace xTRT {

  class NTuplerBase {

  protected:

    bool m_saveHits = false; //!

    int         m_runN;         //!
    long long   m_evtN;         //!
    float       m_avgMu;        //!
    float       m_weight;       //!
    float       m_trkOcc;       //!
    float       m_pT;           //!
    float       m_lep_pT;       //!
    float       m_p;            //!
    float       m_eta;          //!
    float       m_phi;          //!
    float       m_theta;        //!
    float       m_eProbHT;      //!
    int         m_nTRThits;     //!
    int         m_nTRThitsMan;  //!
    int         m_nTRTouts;     //!
    int         m_nTRToutsMan;  //!
    int         m_nHThits;      //!
    int         m_nHThitsMan;   //!
    int         m_nPrechitsMan; //!
    int         m_nArhits;      //!
    int         m_nArhitsMan;   //!
    int         m_nXehits;      //!
    int         m_nXehitsMan;   //!
    float       m_dEdxNoHT;     //!
    int         m_nHitsdEdx;    //!
    float       m_sumL;         //!
    float       m_sumToT;       //!
    float       m_sumToTsumL;   //!
    float       m_fAr;          //!
    float       m_fHTMB;        //!
    float       m_PHF;          //!

    uint  m_HTMB[40];        //!
    uint  m_bitPattern[40];  //!
    uint  m_gasType[40];     //!
    int   m_bec[40];         //!
    int   m_layer[40];       //!
    int   m_strawlayer[40];  //!
    int   m_strawnumber[40]; //!
    float m_drifttime[40];   //!
    float m_tot[40];         //!
    float m_T0[40];          //!
    int   m_type[40];        //!
    float m_localTheta[40];  //!
    float m_localPhi[40];    //!
    float m_HitZ[40];        //!
    float m_HitR[40];        //!
    float m_rTrkWire[40];    //!
    float m_L[40];           //!
    float m_ZR[40];          //!

  public:

    NTuplerBase();
    virtual ~NTuplerBase();

    void clearHits();
    bool fillHitBasedVariables(const xAOD::TrackParticle* track,
                               const xTRT::MSOS* msos,
                               const xTRT::DriftCircle* driftCircle,
                               const bool type0only,
                               const bool isMC);

    ClassDef(xTRT::NTuplerBase, 1);

  };

}

#define CONNECT_BRANCHES(TREE)                                           \
  { TREE->Branch("runN",        &m_runN);                                \
    TREE->Branch("avgmu",       &m_avgMu);                               \
    TREE->Branch("weight",      &m_weight);                              \
    TREE->Branch("trkOcc",      &m_trkOcc);                              \
    TREE->Branch("p",           &m_p);                                   \
    TREE->Branch("pT",          &m_pT);                                  \
    TREE->Branch("lep_pT",      &m_lep_pT);                              \
    TREE->Branch("eta",         &m_eta);                                 \
    TREE->Branch("phi",         &m_phi);                                 \
    TREE->Branch("eProbHT",     &m_eProbHT);                             \
    TREE->Branch("nTRThits",    &m_nTRThits);                            \
    TREE->Branch("nTRThitsMan", &m_nTRThitsMan);                         \
    TREE->Branch("nTRTouts",    &m_nTRTouts);                            \
    TREE->Branch("nArhits",     &m_nArhits);                             \
    TREE->Branch("nXehits",     &m_nXehits);                             \
    TREE->Branch("nHThitsMan",  &m_nHThitsMan);                          \
    TREE->Branch("nPrechitsMan",&m_nPrechitsMan);                        \
    TREE->Branch("NhitsdEdx",   &m_nHitsdEdx);                           \
    TREE->Branch("sumToT",      &m_sumToT);                              \
    TREE->Branch("sumL",        &m_sumL);                                \
    TREE->Branch("sumToTsumL",  &m_sumToTsumL);                          \
    TREE->Branch("fAr",         &m_fAr);                                 \
    TREE->Branch("fHTMB",       &m_fHTMB);                               \
    TREE->Branch("PHF",         &m_PHF);                                 \
    if ( m_saveHits ) {                                                        \
      TREE->Branch("hit_bitPattern", m_bitPattern,   "hit_bitPattern[40]/i");  \
      TREE->Branch("hit_HTMB",       m_HTMB,         "hit_HTMB[40]/i");        \
      TREE->Branch("hit_gasType",    m_gasType,      "hit_gasType[40]/i");     \
      TREE->Branch("hit_bec",        m_bec,          "hit_bec[40]/I");         \
      TREE->Branch("hit_layer",      m_layer,        "hit_layer[40]/I");       \
      TREE->Branch("hit_strawlayer", m_strawlayer,   "hit_strawlayer[40]/I");  \
      TREE->Branch("hit_strawnumber",m_strawnumber,  "hit_strawnumber[40]/I"); \
      TREE->Branch("hit_drifttime",  m_drifttime,    "hit_drifttime[40]/F");   \
      TREE->Branch("hit_tot",        m_tot,          "hit_tot[40]/F");         \
      TREE->Branch("hit_T0",         m_T0,           "hit_T0[40]/F");          \
      TREE->Branch("hit_localTheta", m_localTheta,   "hit_localTheta[40]/F");  \
      TREE->Branch("hit_localPhi",   m_localPhi,     "hit_localPhi[40]/F");    \
      TREE->Branch("hit_HitZ",       m_HitZ,         "hit_HitZ[40]/F");        \
      TREE->Branch("hit_HitR",       m_HitR,         "hit_HitR[40]/F");        \
      TREE->Branch("hit_rTrkWire",   m_rTrkWire,     "hit_rTrkWire[40]/F");    \
      TREE->Branch("hit_L",          m_L,            "hit_L[40]/F");           \
      TREE->Branch("hit_ZR",         m_ZR,           "hit_ZR[40]/F");          \
    } }

#endif
