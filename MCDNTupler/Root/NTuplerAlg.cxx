// ATLAS
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <AsgTools/MessageCheck.h>
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>

// ROOT
#include <TSystem.h>
#include <TFile.h>
#include <TH1F.h>

// MCDNTupler
#include <MCDNTupler/NTuplerAlg.h>

// C++
#include <sstream>

ClassImp(xTRT::NTuplerAlg)

xTRT::NTuplerAlg::NTuplerAlg() : xTRT::TNPAlgorithm(), xTRT::NTuplerBase() {}

xTRT::NTuplerAlg::~NTuplerAlg() {}

EL::StatusCode xTRT::NTuplerAlg::histInitialize() {
  ANA_CHECK_SET_TYPE(EL::StatusCode);
  ANA_CHECK(xTRT::TNPAlgorithm::histInitialize());

  m_saveHits  = config()->getOpt<bool>("StoreHits",false);
  m_type0only = config()->getOpt<bool>("Type0HitOnly",false);

  create(TH1F("h_averageMu","",70,-0.5,69.5));

  SETUP_OUTPUT_TREE(m_tree_invM_el,"invM_el");
  SETUP_OUTPUT_TREE(m_tree_invM_mu,"invM_mu");
  m_tree_invM_el->Branch("m",&m_invM_el);
  m_tree_invM_mu->Branch("m",&m_invM_mu);

  SETUP_OUTPUT_TREE(m_tree_el_tags,"electron_tags");
  SETUP_OUTPUT_TREE(m_tree_el_probes,"electron_probes");
  SETUP_OUTPUT_TREE(m_tree_mu,"muons");
  CONNECT_BRANCHES(m_tree_el_tags);
  CONNECT_BRANCHES(m_tree_el_probes);
  CONNECT_BRANCHES(m_tree_mu);

  SETUP_OUTPUT_TREE(m_tree_el_mc,"electron_mc");
  SETUP_OUTPUT_TREE(m_tree_mu_mc,"muon_mc");
  CONNECT_BRANCHES(m_tree_el_mc);
  CONNECT_BRANCHES(m_tree_mu_mc);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode xTRT::NTuplerAlg::initialize() {
  ANA_CHECK_SET_TYPE(EL::StatusCode);
  ANA_CHECK(xTRT::TNPAlgorithm::initialize());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode xTRT::NTuplerAlg::execute() {
  ANA_CHECK_SET_TYPE(EL::StatusCode);
  ANA_CHECK(xTRT::TNPAlgorithm::execute());
  clearHits();

  if ( not passGRL() ) return EL::StatusCode::SUCCESS;

  float w = eventWeight();
  m_weight = w;

  auto evtinfo = eventInfo();
  m_evtN  = (long long)(evtinfo->eventNumber());
  m_runN  = (int)(evtinfo->runNumber());
  m_avgMu = averageMu();
  grab<TH1F>("h_averageMu")->Fill(m_avgMu,w);

  auto fillFromContainer = [this](const auto* container, TTree* tree2fill) {
    for ( const auto& lepton : *container ) {
      m_lep_pT = lepton->pt();
      auto trk = getTrack(lepton);
      if ( m_lep_pT > 200000 || trk->pt() > 200000 ) continue;
      analyzeTrack(getTrack(lepton),tree2fill);
    }
  };

  if ( isData() ) {
    auto probes = probeElectrons();
    auto tags   = tagElectrons();
    auto muons  = goodMuons();

    for ( const float m : invMassesEl() ) {
      m_invM_el = m*toGeV;
      m_tree_invM_el->Fill();
    }
    for ( const float m : invMassesMu() ) {
      m_invM_mu = m*toGeV;
      m_tree_invM_mu->Fill();
    }

    fillFromContainer(tags,m_tree_el_tags);
    fillFromContainer(probes,m_tree_el_probes);
    fillFromContainer(muons,m_tree_mu);
  } // if data

  if ( isMC() ) {
    auto electrons = selectedElectrons();
    auto muons     = selectedMuons();

    fillFromContainer(electrons,m_tree_el_mc);
    fillFromContainer(muons,m_tree_mu_mc);
  } // if mc

  return EL::StatusCode::SUCCESS;
}

void xTRT::NTuplerAlg::analyzeTrack(const xAOD::TrackParticle* track, TTree* tree2fill) {
  // check for vertex
  auto vtx = track->vertex();
  if ( vtx == nullptr ) return;

  clearHits();

  bool yepMC = isMC();

  uint8_t ntrthits = -1;
  uint8_t nxehits  = -1;
  uint8_t nouthits = -1;
  track->summaryValue(ntrthits,xAOD::numberOfTRTHits); // type 0 only!
  track->summaryValue(nxehits, xAOD::numberOfTRTXenonHits);
  track->summaryValue(nouthits,xAOD::numberOfTRTOutliers);

  m_nTRThits = ntrthits; // type 0 only
  m_nTRTouts = nouthits;
  m_nXehits  = nxehits;  // all xenon hits
  m_nArhits  = ntrthits + nouthits -nxehits; // all argon hits
  m_nTRThitsMan = 0;
  m_nHThitsMan  = 0;
  m_sumL        = 0.0;
  m_sumToT      = 0.0;
  m_sumToTsumL  = 0.0;

  m_pT        = track->pt();
  m_p         = track->p4().P();
  m_eta       = track->eta();
  m_phi       = track->phi();
  m_theta     = track->theta();

  m_trkOcc  = get(xTRT::Acc::TRTTrackOccupancy,track);
  m_eProbHT = get(xTRT::Acc::eProbabilityHT,track);

  const xTRT::MSOS*        msos        = nullptr;
  const xTRT::DriftCircle* driftCircle = nullptr;
  for ( const auto& trackMeasurement : xTRT::Acc::msosLink(*track) ) {
    if ( trackMeasurement.isValid() ) {
      msos = *trackMeasurement;
      if ( msos->detType() != 3 ) continue; // TRT hits only.
      if ( !(msos->trackMeasurementValidationLink().isValid()) ) continue;
      driftCircle = *(msos->trackMeasurementValidationLink());
      if ( !fillHitBasedVariables(track,msos,driftCircle,m_type0only,yepMC) ) continue;
    }
  }

  m_fHTMB = (float)m_nHThitsMan / (m_nTRThits+m_nTRTouts);
  m_fAr   = (float)m_nArhits / (m_nTRThits+m_nTRTouts);
  m_PHF   = (float)m_nPrechitsMan / (m_nTRThits+m_nTRTouts);
  m_sumToTsumL = m_sumToT/m_sumL;

  tree2fill->Fill();
}
