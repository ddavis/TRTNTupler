// ATLAS
#include <EventLoop/StatusCode.h>
#include <AsgTools/MessageCheck.h>

// ROOT
#include <TSystem.h>
#include <TTree.h>

// TRTFramework / NTupler
#include <TRTFramework/Algorithm.h>
#include <MCDNTupler/NTuplerBase.h>

// C++
#include <sstream>

ClassImp(xTRT::NTuplerBase)

xTRT::NTuplerBase::NTuplerBase() {}

xTRT::NTuplerBase::~NTuplerBase() {}

void xTRT::NTuplerBase::clearHits() {
  m_nTRThitsMan = 0;
  m_nPrechitsMan = 0;
  std::fill(m_HTMB,m_HTMB+40,99999);
  std::fill(m_bitPattern,m_bitPattern+40,99999);
  std::fill(m_gasType,m_gasType+40,99999);
  std::fill(m_bec,m_bec+40,99999);
  std::fill(m_layer,m_layer+40,99999);
  std::fill(m_strawlayer,m_strawlayer+40,99999);
  std::fill(m_strawnumber,m_strawnumber+40,99999);
  std::fill(m_drifttime,m_drifttime+40,99999);
  std::fill(m_tot,m_tot+40,99999);
  std::fill(m_T0,m_T0+40,99999);
  std::fill(m_type,m_type+40,99999);
  std::fill(m_localTheta,m_localTheta+40,99999);
  std::fill(m_localPhi,m_localPhi+40,99999);
  std::fill(m_HitZ,m_HitZ+40,99999);
  std::fill(m_HitR,m_HitR+40,99999);
  std::fill(m_rTrkWire,m_rTrkWire+40,99999);
  std::fill(m_ZR,m_ZR+40,99999);
  std::fill(m_L,m_L+40,99999);
}

bool xTRT::NTuplerBase::fillHitBasedVariables(const xAOD::TrackParticle* track,
                                              const xTRT::MSOS* msos,
                                              const xTRT::DriftCircle* driftCircle,
                                              const bool type0only,
                                              const bool isMC) {
  auto hit = xTRT::getHitSummary(track,msos,driftCircle,isMC);
  if ( hit.tot < 0 || hit.L < 0 ) return false;
  if ( type0only && (hit.type != 0) ) return false;
  if ( hit.HTMB ) m_nHThitsMan++;

  if ( hit.tot > 0.005 && hit.L > 0.00005 ) {
    m_sumL += hit.L;
    m_sumToT += hit.tot;
  }

  if ( m_nTRThitsMan < 40 ) {
    m_type[m_nTRThitsMan]        = hit.type;
    m_bitPattern[m_nTRThitsMan]  = hit.bitPattern;
    m_HTMB[m_nTRThitsMan]        = hit.HTMB;
    m_gasType[m_nTRThitsMan]     = hit.gasType;
    m_bec[m_nTRThitsMan]         = hit.bec;
    m_layer[m_nTRThitsMan]       = hit.layer;
    m_strawlayer[m_nTRThitsMan]  = hit.strawlayer;
    m_strawnumber[m_nTRThitsMan] = hit.strawnumber;
    m_drifttime[m_nTRThitsMan]   = hit.drifttime;
    m_tot[m_nTRThitsMan]         = hit.tot;
    m_T0[m_nTRThitsMan]          = hit.T0;
    m_localTheta[m_nTRThitsMan]  = hit.localTheta;
    m_localPhi[m_nTRThitsMan]    = hit.localPhi;
    m_HitZ[m_nTRThitsMan]        = hit.HitZ;
    m_HitR[m_nTRThitsMan]        = hit.HitR;
    m_rTrkWire[m_nTRThitsMan]    = hit.rTrkWire;
    m_ZR[m_nTRThitsMan]          = hit.ZR;
    m_L[m_nTRThitsMan]           = hit.L;
  }

  if ( hit.isPrec ) m_nPrechitsMan++;
  m_nTRThitsMan++;

  return true;

  /*
    m_type.push_back(hit.type);
    m_HTMB.push_back(hit.HTMB);
    m_gasType.push_back(hit.gasType);
    m_bec.push_back(hit.bec);
    m_layer.push_back(hit.layer);
    m_strawlayer.push_back(hit.strawlayer);
    m_strawnumber.push_back(hit.strawnumber);
    m_drifttime.push_back(hit.drifttime);
    m_tot.push_back(hit.tot);
    m_T0.push_back(hit.T0);

    m_localTheta.push_back(hit.localTheta);
    m_localPhi.push_back(hit.localPhi);
    m_HitZ.push_back(hit.HitZ);
    m_HitR.push_back(hit.HitR);
    m_rTrkWire.push_back(hit.rTrkWire);
    m_L.push_back(hit.L);
  */
}
