#include <MCDNTupler/NTuplerAlg.h>
#include <TRTFramework/Runner.h>

int main(int argc, char **argv) {
  xTRT::NTuplerAlg *alg = new xTRT::NTuplerAlg();
  return xTRT::Runner(argc,argv,alg);
}
