# TRTNTupler

A package that uses TRTFramework to create ntuples.

Packages:
- TRTFramework (a git submodule): the framework/API providing a ncie
  environment for TRT analyses.
- MCDNTupler: The "main" package, has algorithm for Data and MC
  processing and base ntuple structure class.
  
